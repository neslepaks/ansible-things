import io

from ansible.errors import AnsibleFilterError, AnsibleUndefinedVariable
from ansible.module_utils._text import to_native
import jinja2.exceptions


quote_all_values = False

def build_node(fp, node, indent):
    if node is None:
        return
    elif isinstance(node, list):
        for child in node:
            build_node(fp, child, indent)
    elif isinstance(node, dict):
        for child in node.keys():
            if isinstance(node[child], (dict, list, type(None))):
                print("{}{} {{".format('    '*indent, child), file=fp)
                build_node(fp, node[child], indent+1)
                print("{}}}".format('    '*indent), file=fp)
            else:
                if quote_all_values or (isinstance(node[child], str) and (node[child] == "" or ' ' in node[child])):
                    node[child] = '"{}"'.format(node[child])
                print("{}{} {}".format('    '*indent, child, node[child]), file=fp)
    elif isinstance(node, str):
        if node[0:2] in ('d=', 'r=', 'a='):
            # Looks like our special compact firewall rule syntax, apply filter
            build_node(fp, maybe_expand_rule(node), indent)
        else:
            print("{}{}".format('    '*indent, node), file=fp)
    else:
        print("{}{}".format('    '*indent, node), file=fp)


def vyedge_build_config(config):
    fp = io.StringIO()
    build_node(fp, config, 0)
    return fp.getvalue()


def vyedge_build_config_quote(config):
    global quote_all_values
    quote_all_values = True
    return vyedge_build_config(config)


NAME_MAP = {
    'a': 'accept',
    'd': 'drop',
    'r': 'reject',
    's': 'state',
    'desc': 'description',
    'daddr': ('destination', 'address'),
    'dagr': ('destination', 'group', 'address-group'),
    'dngr': ('destination', 'group', 'network-group'),
    'dport': ('destination', 'port'),
    'proto': 'protocol',
    'saddr': ('source', 'address'),
    'sagr': ('source', 'group', 'address-group'),
    'sngr': ('source', 'group', 'network-group'),
    'sport': ('source', 'port'),
}


def parse_rule(rule):
    result = {}
    for key, val in [r.split('=') for r in rule.split()]:
        realname = NAME_MAP.get(key, key)
        if isinstance(realname, tuple):
            obj = result
            for name in realname:
                obj.setdefault(name, {})
                last_obj = obj
                obj = obj[name]
            last_obj[name] = val
        else:
            if realname in ('accept', 'drop', 'reject'):
                number = val
                result['action'] = realname
            elif realname == 'state':
                result.setdefault('state', {})
                if val == 'inv':
                    result['state']['invalid'] = 'enable'
                elif val == 'est-rel':
                    result['state']['established'] = 'enable'
                    result['state']['related'] = 'enable'
                else:
                    raise KeyError(val)
            elif realname == 'description':
                idx = rule.find('desc=') + 5
                result[realname] = rule[idx:]
                break
            else:
                result[realname] = val
    return [{f'rule {number}': dict(sorted(result.items()))}]


def maybe_expand_rule(thing):
    try:
        if not isinstance(thing, str):
            return thing
        return parse_rule(thing)
    except jinja2.exceptions.UndefinedError as err:
        raise AnsibleUndefinedVariable(
            "Something happened, this was the original exception: %s" % to_native(err)
        ) from err
    except Exception as err:
        raise AnsibleFilterError(
            "Something happened, this was the original exception: %s" % to_native(err)
        ) from err


class FilterModule:
    def filters(self):
        return {
            'vyedge_expand_fw_rule': maybe_expand_rule,
            'vyedge_build_config': vyedge_build_config,
            'vyedge_build_config_quote': vyedge_build_config_quote
        }
