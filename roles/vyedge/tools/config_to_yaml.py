import sys

problematic_yaml_values = (
    'y|Y|yes|Yes|YES|n|N|no|No|NO'
    '|true|True|TRUE|false|False|FALSE'
    '|on|On|ON|off|Off|OFF'
).split('|')


def sanitize(val):
    return f'"{val}"' if val in problematic_yaml_values else val


def main():
    print("vyedge_config:")
    indent = 0
    for line in sys.stdin:
        line = line.strip()
        if len(line) == 0 or line.startswith('/*'):
            continue
        try:
            if line.endswith('{'):
                print('  '*indent + ' - ' + line[:-2] + ':')
                indent += 1
            elif line.endswith('}'):
                indent -= 1
            elif ' ' in line:
                idx = line.index(' ')
                print('  '*indent + ' - ' + line[:idx] +
                      ': ' + sanitize(line[idx+1:]))
            else:
                print('  '*indent + ' - ' + line)
        except Exception as e:
            sys.exit(f'Problem with line "{line}": {e}')


if __name__ == '__main__':
    main()
