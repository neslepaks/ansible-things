vyedge
======

Role that can configure a VyOS or EdgeOS router.  The key idea of this role is that it does not use the vyos or edgeos network modules, but instead treats these as regular Linux machines using a normal ssh connection and the installed python interpreter.  I find this approach to be less brittle and prone to incompatibilities over time than using the network modules.

The drawback is that the way it works is by replacing the whole config with the one we template in.  Therefore it is not a good choice for controlling a partial config.  All or nothing here.

To get started with an existing config you can take a config backup and run it through the included `config_to_yaml.py` script.  If you run the result against the host then you should ideally not see a diff.

Nothing will be committed on the router until the `commit_message` variable is set.  By default the role will display a diff and throw an error if there is one.

Role Variables
--------------

- `vyedge_config` -- This contains the entire configuration as a yaml tree.  See example below.  Note that it should be kept in sorted order or you will get a diff every time.
- `vyedge_quote_all_values` -- Set this to true if you have a newer VyOS that likes to keep single-word values in quotes.
- `vyedge_tmp_config_path` -- Where on the router to write the temp config.
- `commit_message` -- Set this to commit and save the changes on the router.

Example
-------

An example config might look like:

```
vyedge_config:
  - interfaces:
      ethernet eth0:
        address: 192.168.0.1/24
        hw-id: "00:e2:69:..."
  - service:
      ntp:
        allow-client:
          - address: "0.0.0.0/0"
          - address: "::/0"
        server time1.vyos.net: {}
        server time2.vyos.net: {}
        server time3.vyos.net: {}
      ssh:
        port: 22
  - system:
      - config-management:
          commit-revisions: 100
      - console:
          device ttyS0:
            speed: 115200
      - host-name: vyos
      - login:
          user vyos:
            authentication:
              encrypted-password: ...
              plaintext-password: ""
              public-keys ansible@ansible:
                key: ...
                type: ssh-ed25519
      - syslog:
          global:
            facility all:
              level: info
            facility local7:
              level: debug
```

The config builder doesn't much care if config nodes are objects with many keys or lists of single-key objects, but sometimes you have repeating node names so lists are required.  The `config_to_yaml.py` script uses lists for everything for this reason.

Compact firewall rule syntax
----------------------------

The role contains a filter for expressing firewall rules in a compact one-line format.  For example, this syntax:

```
- a=10 daddr=10.0.0.2-10.0.0.3 dport=53 log=disable proto=tcp_udp desc=Allow DNS to pihole
```

will be expanded to something like:

```
- rule 10:
  - action: accept
  - description: "Allow DNS to pihole"
  - destination:
    - address: "10.0.0.2-10.0.0.3"
    - port: 53
  - log: disable
  - protocol: tcp_udp
```

which ultimately ends up as:

```
        rule 10 {
            action accept
            description "Allow DNS to pihole"
            destination {
                address 10.0.0.2-10.0.0.3
                port 53
            }
            log disable
            protocol tcp_udp
        }
```

Consult the source for more possibilities.

License
-------

BSD
